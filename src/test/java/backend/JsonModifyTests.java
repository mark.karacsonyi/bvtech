package backend;

import backend.model.footballDTO.FootballStats;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.Helpers;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JsonModifyTests {

    Logger logger = Logger.getLogger(JsonModifyTests.class.getName());

    @Test
    public void readFromFile() throws IOException {
        logger.setLevel(Level.INFO);

        ObjectMapper mapper = new ObjectMapper();
        FootballStats footballStats = mapper
                .readValue(new File("src/main/resources/data/jsonExamples/footballStat.json"), FootballStats.class);

        logger.info(("\n" + "CornerScore away: " + footballStats.getCorners().getScore().getAway()
                + "\n" + "CornerScore home: " + footballStats.getCorners().getScore().getHome()) + "\n");

        logger.info(("\n" + "FoulsScore away: " + footballStats.getFouls().getScore().getAway()
                + "\n" + "FoulsScore home: " + footballStats.getCorners().getScore().getHome()) + "\n");

        logger.info(("\n" + "GoalKicksScore away: " + footballStats.getGoalKicks().getScore().getAway()
                + "\n" + "GoalKicksScore home: " + footballStats.getGoalKicks().getScore().getHome()) + "\n");

        logger.info(("\n" + "ThrowInsScore away: " + footballStats.getThrowIns().getScore().getAway()
                + "\n" + "ThrowInsScore home: " + footballStats.getThrowIns().getScore().getHome()) + "\n");

        logger.info(("\n" + "GoalsScore away: " + footballStats.getGoals().getScore().getAway()
                + "\n" + "GoalsScore home: " + footballStats.getGoals().getScore().getHome()) + "\n");
    }

    @Test
    public void modifyJson() throws IOException, ParseException {

        ObjectMapper mapper = new ObjectMapper();
        FootballStats footballStats = mapper
                .readValue(new File("src/main/resources/data/jsonExamples/footballStat.json"), FootballStats.class);

        footballStats.setFixtureId("1000");
        footballStats.setCustomerId(1);
        footballStats.getStartTimes().setFirstHalf(Helpers.convertAndModifyStringDate(footballStats.getStartTimes().getFirstHalf(), -1, -30));
        footballStats.getStartTimes().setSecondHalf(Helpers.convertAndModifyStringDate(footballStats.getStartTimes().getSecondHalf(), 0, -30));
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(footballStats));
    }
}
