package backend;

import backend.rest.CurrenciesRest;
import com.fasterxml.jackson.core.JsonProcessingException;
import common.Helpers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CurrenciesListTests {

    public String jsonAsString;

    @BeforeClass
    public void queryCurrencyList() {
        jsonAsString = CurrenciesRest.queryCurrencyResponse().asString();
    }

    @Test
    public void currencyListHasOver20Items() throws JsonProcessingException {
        Assert.assertTrue(Helpers.jsonToMapConverter(jsonAsString).size() > 20,
                "List has less then 20 item");
    }

    @Test
    public void britishPoundIsListed() throws JsonProcessingException {
        Assert.assertTrue(Helpers.jsonToMapConverter(jsonAsString).containsValue("British Pound"),
                " British Pound is not part of the list");
    }

    @Test
    public void UnitedStatesDollarIsListed() throws JsonProcessingException {
        Assert.assertTrue(Helpers.jsonToMapConverter(jsonAsString).containsValue("United States dollar"),
                "United States dollar is not part of the list");
    }

 //  @Test
 //  public void testWithRestAssure() {
 //      Assert.assertTrue(CurrenciesRest.queryCurrencyResponse().jsonPath().getMap(".").size() > 20);
 //  }

    @Test
    @SuppressWarnings("rawtypes")
    public void checkListSize() throws JsonProcessingException {
        Map resultMap = Helpers.jsonToMapConverter(jsonAsString);
        List<String> abbreviationsList = new ArrayList<>(resultMap.keySet());
        Assert.assertEquals(abbreviationsList.size(), resultMap.size(), "Abbreviation list size differ from actual list size");
        Collections.sort(abbreviationsList);

        int sizeOfExchangeList = 0;
        for (String s : abbreviationsList) {
            if (s.equals(abbreviationsList.get(0))) {
                sizeOfExchangeList = CurrenciesRest.querySomething(s).jsonPath().getMap(s).size();
            }
            Assert.assertEquals(sizeOfExchangeList,
                    CurrenciesRest.querySomething(s).jsonPath().getMap(s).size());
        }
    }
}
