package backend;

import backend.model.betResponseDTO.Root;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import common.Helpers;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class XmlModifyTests {

    private Root root;

    @BeforeMethod
    public void loadXmlFromFile() throws IOException {
        File file = new File("src/main/resources/data/xmlExamples/betResponse.xml");
        XmlMapper xmlMapper = new XmlMapper();
        String xml = Helpers.inputStreamToString((new FileInputStream(file)));
        root = xmlMapper.readValue(xml, Root.class);
    }

    @Test
    public void readXMLValues() {
        System.out.println(root.getBetslip().getBetSaveResponses().getElement().getSportsBookReference());
        System.out.println(root.getBetslip().getBetSaveResponses().getElement().getTransactionId());
        System.out.println(root.getBetslip().getBets().getSingles().getElement().getOutcomeId());
        System.out.println(root.getBetslip().getSelections().getElement().getOutcomeId());
        System.out.println(root.getBetslip().getBets().getSingles().getElement().getTotalStake());

    }


    @Test
    public void modifyXMLValues() throws JsonProcessingException {
        root.getBetslip().getBetSaveResponses().getElement().setBetDescription(RandomStringUtils.randomAlphabetic(100));
        root.getBetslip().getBets().getSingles().getElement().setTotalStake(10);
        root.getBetslip().getBetSaveResponses().getElement().setTransactionId("1");
        ObjectMapper xmlMapper = new ObjectMapper();
        System.out.println(xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));

    }

}
