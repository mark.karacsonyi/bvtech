package frontend;

import common.Configuration;
import common.DatabaseConnection;
import frontend.Pages.DropDownGridContainer;
import frontend.Pages.NestedGridContainer;
import frontend.Pages.SideNavigation;
import frontend.Pages.TileSortAndFilteringContainer;
import frontend.utilities.WebDriverManager;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class FrontEndTests {

    private WebDriver webDriver;
    StopWatch stopWatch;
    private Connection connection;


    @BeforeMethod
    public void setUp() {
        connection = DatabaseConnection.connect();
        stopWatch = new StopWatch();
        stopWatch.start();
        WebDriverManager webDriverManager = new WebDriverManager();
        webDriver = webDriverManager.getWebDriver();
        webDriver.get(Configuration.getWebUrl());
    }

    @AfterMethod
    public void tearDown(ITestResult result) throws SQLException {
        webDriver.close();
        stopWatch.stop();
        Assert.assertTrue(stopWatch.getTime() < DatabaseConnection.executeQuery(connection, result.getMethod().getMethodName()),"Previous Run was quicker");
        DatabaseConnection.executeUpdate(connection, result.getMethod().getMethodName(), stopWatch.getTime());
        DatabaseConnection.closeConnection(connection);
    }

    @Test
    public void tileFilteringTest() {

        SideNavigation sideNavigation = new SideNavigation(webDriver);
        sideNavigation.clickOnTileSortAndFiltering();

        TileSortAndFilteringContainer tileSortAndFilteringContainer = new TileSortAndFilteringContainer(webDriver);
        tileSortAndFilteringContainer.disposeOfAnnoyingPopUp();
        tileSortAndFilteringContainer.sendWordsToAnimalInputField("a")
                .navigateOnSlider()
                .clickOnDropDown(2)
                .checkAscendingBox();

        Assert.assertTrue(tileSortAndFilteringContainer.checkResult() > 12);

    }

    @Test
    public void dropDownGridCategoryTest() {
        SideNavigation sideNavigation = new SideNavigation(webDriver);
        sideNavigation.clickOnDropDownGrid();

        DropDownGridContainer dropDownGridContainer = new DropDownGridContainer(webDriver);
        dropDownGridContainer.disposeOfAnnoyingPopUp();
        dropDownGridContainer.selectDropDown();
        dropDownGridContainer.checkListAndSelectAppropriateOption("Exercise", "Ea", 1.1);
    }

    @Test
    public void nestedGridTest() throws InterruptedException {

        SideNavigation sideNavigation = new SideNavigation(webDriver);
        sideNavigation.clickOnNestedGrid();

        NestedGridContainer nestedGridContainer = new NestedGridContainer(webDriver);
        nestedGridContainer.disposeOfAnnoyingPopUp();

        nestedGridContainer.updateSubItemsInSelectedMainItems("Poster Strips", "Correction");

    }

}
