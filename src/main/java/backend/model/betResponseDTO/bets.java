package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class bets {
    private Object multiples;
    private Singles singles;
}
