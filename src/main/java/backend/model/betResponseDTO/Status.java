package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Status {

    private int errorCode;
    private Object extraInfo;
    private boolean success;
}
