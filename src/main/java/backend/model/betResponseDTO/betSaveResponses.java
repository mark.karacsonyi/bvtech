package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class betSaveResponses {
	private BetSaveResponsesElement element;
}
