package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SelectionsElement {

	private boolean fixedPrice;
    private String outcomeId;
    private boolean priceChanged;
    private double priceDecimal;
    private String priceFormat;
    private String priceFormatted;
    private double requestPriceDecimal;
    private String requestPriceFormatted;

}
