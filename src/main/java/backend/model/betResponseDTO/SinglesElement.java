package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SinglesElement {
    private int betTypeId;
    private String description;
    private boolean ew;
    private String outcomeId;
    private boolean priceChanged;
    private double rmfWithCurrPrice;
    private double rmfWithReqPrice;
    private int totalStake;

}
