package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Root {
	private Betslip betslip;
	private Status status;
}
