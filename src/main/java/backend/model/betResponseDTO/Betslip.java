package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Betslip {
	private betSaveResponses betSaveResponses;
	private bets bets;
	private int id;
	private String reference;
	private Selections selections;
}
