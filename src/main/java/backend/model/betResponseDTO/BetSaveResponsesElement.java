package backend.model.betResponseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BetSaveResponsesElement {

    private String betDescription;
    private int code;
    private String description;
    private int failedTransactionId;
    private String sportsBookReference;
    private String transactionId;
}
