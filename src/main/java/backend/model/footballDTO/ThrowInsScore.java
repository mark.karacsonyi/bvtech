
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ThrowInsScore {

    @JsonProperty("Away")
    private Integer away;

    @JsonProperty("Home")
    private Integer home;

}
