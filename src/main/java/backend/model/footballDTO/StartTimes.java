
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StartTimes {

    @JsonProperty("FirstHalf")
    private String firstHalf;

    @JsonProperty("SecondHalf")
    private String secondHalf;

}
