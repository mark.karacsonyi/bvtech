
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoppageTimes {

    @JsonProperty("FirstHalf")
    private String firstHalf;

}
