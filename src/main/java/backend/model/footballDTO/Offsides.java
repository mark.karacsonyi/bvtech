
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Offsides {

    @JsonProperty("IsCollected")
    private Boolean isCollected;

    @JsonProperty("IsReliable")
    private Boolean isReliable;

    @JsonProperty("Score")
    private OffsidesScore score;

}
