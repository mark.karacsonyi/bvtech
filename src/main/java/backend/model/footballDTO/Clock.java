
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Clock {

    @JsonProperty("IsClockRunning")
    private Boolean isClockRunning;

    @JsonProperty("TimeElapsedInPhase")
    private String timeElapsedInPhase;

    @JsonProperty("TimestampUtc")
    private String timestampUtc;

}
