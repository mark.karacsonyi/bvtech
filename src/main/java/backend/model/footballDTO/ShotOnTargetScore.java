
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShotOnTargetScore {

    @JsonProperty("Away")
    private Integer away;

    @JsonProperty("Home")
    private Integer home;

}
