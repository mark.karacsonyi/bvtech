
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FootballStats {

    @JsonProperty("BlockedShots")
    private BlockedShots blockedShots;
    @JsonProperty("CanGoStraightToPenaltiesAfterNormalTime")
    private Boolean canGoStraightToPenaltiesAfterNormalTime;
    @JsonProperty("CanGoToExtraTime")
    private Boolean canGoToExtraTime;
    @JsonProperty("CanGoToPenalties")
    private Boolean canGoToPenalties;
    @JsonProperty("Clock")
    private Clock clock;
    @JsonProperty("Corners")
    private Corners corners;
    @JsonProperty("CurrentBookingState")
    private String currentBookingState;
    @JsonProperty("CurrentDangerState")
    private String currentDangerState;
    @JsonProperty("CurrentPhase")
    private String currentPhase;
    @JsonProperty("CurrentVarState")
    private String currentVarState;
    @JsonProperty("CustomerId")
    private Integer customerId;
    @JsonProperty("ExtraTimeHalfDuration")
    private String extraTimeHalfDuration;
    @JsonProperty("FixtureId")
    private String fixtureId;
    @JsonProperty("Fouls")
    private Fouls fouls;
    @JsonProperty("GoalKicks")
    private GoalKicks goalKicks;
    @JsonProperty("Goals")
    private Goals goals;
    @JsonProperty("IsSecondLeg")
    private Boolean isSecondLeg;
    @JsonProperty("MessageTimestampUtc")
    private String messageTimestampUtc;
    @JsonProperty("MissedPenalties")
    private MissedPenalties missedPenalties;
    @JsonProperty("NormalTimeHalfDuration")
    private String normalTimeHalfDuration;
    @JsonProperty("Offsides")
    private Offsides offsides;
    @JsonProperty("PenaltiesAwarded")
    private PenaltiesAwarded penaltiesAwarded;
    @JsonProperty("SavedPenalties")
    private SavedPenalties savedPenalties;
    @JsonProperty("SecondYellowCards")
    private SecondYellowCards secondYellowCards;
    @JsonProperty("ShotsOffTarget")
    private ShotsOffTarget shotsOffTarget;
    @JsonProperty("ShotsOffWoodwork")
    private ShotsOffWoodwork shotsOffWoodwork;
    @JsonProperty("ShotsOnTarget")
    private ShotsOnTarget shotsOnTarget;
    @JsonProperty("StartTimes")
    private StartTimes startTimes;
    @JsonProperty("StoppageTimes")
    private StoppageTimes stoppageTimes;
    @JsonProperty("StraightRedCards")
    private StraightRedCards straightRedCards;
    @JsonProperty("Substitutions")
    private Substitutions substitutions;
    @JsonProperty("ThrowIns")
    private ThrowIns throwIns;
    @JsonProperty("YellowCards")
    private YellowCards yellowCards;

}
