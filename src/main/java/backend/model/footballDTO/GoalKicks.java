
package backend.model.footballDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GoalKicks {

    @JsonProperty("IsCollected")
    private Boolean isCollected;

    @JsonProperty("IsReliable")
    private Boolean isReliable;

    @JsonProperty("Score")
    private GoalKicksScore score;

}
