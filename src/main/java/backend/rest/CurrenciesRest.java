package backend.rest;

import common.Configuration;
import io.restassured.RestAssured;
import io.restassured.response.Response;


public class CurrenciesRest {

    public static Response queryCurrencyResponse() {

        RestAssured.urlEncodingEnabled = false;

        return RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .log().all()
                .get(Configuration.getApiUrl() + "/currencies.json")
                .then()
                .log().all()
                .assertThat().statusCode(200)
                .extract().response();

    }

    public static Response querySomething(String queryParam) {

        RestAssured.urlEncodingEnabled = false;

        return RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .log().all()
                .get(Configuration.getApiUrl() + "/currencies/" + queryParam + ".json")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .extract().response();

    }

}
