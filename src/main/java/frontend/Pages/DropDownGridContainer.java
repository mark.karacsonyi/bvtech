package frontend.Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class DropDownGridContainer extends PageBase {

    public DropDownGridContainer(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBys(
            @FindBy(xpath = "//div[@class='selectItemLiteText']")
    )
    private List<WebElement> liteTextList;

    @FindBys(
            @FindBy(xpath = "//table[@class='listTable']/tbody/tr[@role='option']")
    )
    private List<WebElement> optionList;

    public void selectDropDown() {
        liteTextList.get(2).click();
    }

    public void checkListAndSelectAppropriateOption(String itemName, String unit, Double cost) {
        boolean stateElement = true;
        while (stateElement)
            try {

                for (WebElement w : optionList) {
                    ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", w);
                    if (w.findElement(By.xpath("td/div")).getText().contains(itemName) &&
                            w.findElement(By.xpath("td[2]/div")).getText().contains(unit) &&
                            Double.parseDouble(w.findElement(By.xpath("td[3]/div")).getText()) > cost) {
                        w.click();
                        break;
                    }
                    stateElement = false;
                }
            } catch (StaleElementReferenceException e) {
                stateElement = true;
            }

    }
}
