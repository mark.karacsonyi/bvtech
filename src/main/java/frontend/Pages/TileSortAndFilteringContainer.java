package frontend.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TileSortAndFilteringContainer extends PageBase {


    public TileSortAndFilteringContainer(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "isc_EQ")
    private WebElement inputField;

    @FindBy(id = "isc_FE")
    private WebElement dropDown;

    @FindBy(xpath = "//table[@class='listTable']/tbody/tr[@role='option'][2]")
    private WebElement lifeSpan;

    @FindBy(id = "isc_FO")
    private WebElement ascendingCheckBox;

    @FindBys(@FindBy(xpath = "//div[@class='tileGrid']/div[class='simpleTile']"))
    private List<WebElement> filteredAnimals;

    @FindBy(id = "isc_EE")
    private WebElement sliderDiv;

    @FindBy(id = "isc_EA")
    private WebElement slider;


    public TileSortAndFilteringContainer sendWordsToAnimalInputField(String words) {
        inputField.clear();
        inputField.sendKeys(words);
        return new TileSortAndFilteringContainer(webDriver);
    }

    public TileSortAndFilteringContainer navigateOnSlider() {
        sliderDiv.click();
        for (int i = 0; i < 20; i++) {
            slider.sendKeys(Keys.ARROW_LEFT);
        }
        for (int i = 0; i < 13; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }

        return new TileSortAndFilteringContainer(webDriver);
    }

    public TileSortAndFilteringContainer clickOnDropDown(Integer i) {
        dropDown.click();
        webDriver.findElement(By.xpath("//tr[@role='option'][" + i + "]")).click();
        return new TileSortAndFilteringContainer(webDriver);
    }

    public TileSortAndFilteringContainer checkAscendingBox() {
        waitElementToBeClickable(ascendingCheckBox);
        ascendingCheckBox.click();
        return new TileSortAndFilteringContainer(webDriver);
    }


    public Integer checkResult() {
        int counter = 0;
        List<WebElement> resultList = webDriver.findElements(By.xpath("//div[@class='tileGrid']/div[@class='simpleTile']"));
        for (WebElement w : resultList) {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", w);
            if (!w.getAttribute("style").contains("visibility: hidden")) {
                counter++;
            }
        }
        return counter;
    }

}


