package frontend.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {

    protected WebDriver webDriver;
    protected JavascriptExecutor javascriptExecutor;
    private WebDriverWait wait;

    public PageBase(WebDriver webDriver) {
        this.webDriver = webDriver;
        javascriptExecutor = (JavascriptExecutor) webDriver;
        wait = new WebDriverWait(webDriver, 5);
        waitUntilPageIsRendered();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//a[@class='iAgreeButton']")
    private WebElement agree;

    public void disposeOfAnnoyingPopUp(){
        agree.click();
    }

    public void waitUntilPageIsRendered() {
        wait.until(webDriver -> ((JavascriptExecutor) webDriver)
                .executeScript("return document.readyState").equals("complete"));
    }

    protected void waitVisibilityOfElement(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

}
