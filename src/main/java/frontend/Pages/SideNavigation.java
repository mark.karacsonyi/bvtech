package frontend.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SideNavigation extends PageBase {

    public SideNavigation(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "isc_SideNavTree_0_valueCell8")
    private WebElement tileSortAndFiltering;

    @FindBy(id = "isc_SideNavTree_0_valueCell16")
    private WebElement dropDownGrid;

    @FindBy(id = "isc_SideNavTree_0_valueCell7")

    private WebElement nestedGrid;

    public void clickOnTileSortAndFiltering() {
        tileSortAndFiltering.click();
    }

    public void clickOnDropDownGrid() {
        dropDownGrid.click();
    }

    public void clickOnNestedGrid(){
        nestedGrid.click();
    }
}
