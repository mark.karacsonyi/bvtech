package frontend.Pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NestedGridContainer extends PageBase {

    public NestedGridContainer(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBys(
            @FindBy(xpath = "//tr[@role='listitem']/td[3]/div")
    )
    private List<WebElement> subItemList;

    @FindBys(
            @FindBy(xpath = "//div[@class='gridBody']/div/table/tbody/tr[@role='listitem']")
    )
    private List<WebElement> optionList;

    @FindBy(xpath = "//tr[@role='listitem' and @aria-selected='true']/td[2]")
    private WebElement currentItemName;

    @FindBy(xpath = "//tr[@role='listitem' and @aria-selected='true']/td[1]")
    private WebElement currentItemOpener;

    @FindBy(xpath = "//div[@role='button' and @aria-label='Save']")
    private WebElement saveButton;

    @FindBy(xpath = "//div[@role='button' and @aria-label='Close']")
    private WebElement closeButton;


    public NestedGridContainer moveDown() {
        Actions actions = new Actions(webDriver);
        actions.sendKeys(Keys.ARROW_DOWN).build().perform();
        return new NestedGridContainer(webDriver);
    }

    public NestedGridContainer clickOnCloseButton() {
        closeButton.click();
        return new NestedGridContainer(webDriver);
    }

    public NestedGridContainer clickOnSaveButton() {
        saveButton.click();
        return new NestedGridContainer(webDriver);
    }

    public NestedGridContainer clickOnFirstItemInList() {
        optionList.get(0).click();
        return new NestedGridContainer(webDriver);
    }

    public void updateSubItemsInSelectedMainItems(String lastItem, String search) throws InterruptedException {
        clickOnFirstItemInList();

        int counter = 1;
        while (!currentItemName.getText().equals(lastItem)) {
            waitVisibilityOfElement(currentItemName);
            currentItemName.click();
            moveDown();
            if (currentItemName.getText().contains(search)) {
                currentItemOpener.click();
                changeSubItemDescriptions(counter);
                counter += subItemList.size();
                if (!closeButton.isDisplayed()) {
                    scrollTo(closeButton);
                }
                clickOnCloseButton();
            }

        }
    }

    public NestedGridContainer scrollTo(WebElement w) {
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", w);
        return new NestedGridContainer(webDriver);
    }

    public NestedGridContainer changeSubItemDescriptions(Integer counter) throws InterruptedException {

        for (int i = 0; i <= subItemList.size() - 1; i++) {
            if (subItemList.size() == 0) {
                break;
            }
            subItemList = webDriver.findElements(By.xpath("//tr[@role='listitem']/td[3]/div"));
            subItemList.get(i).click();
            Actions actions = new Actions(webDriver);
            int number = counter + i;
            Thread.sleep(1000);
              actions.sendKeys(number + " " + (RandomStringUtils.randomAlphabetic(10))).build().perform();
            scrollTo(saveButton);
            clickOnSaveButton();
            scrollTo(currentItemOpener);
        }
        return new NestedGridContainer(webDriver);
    }
}






