package frontend.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

public class WebDriverBuilder {

    private static DesiredCapabilities capabilities;
    private static WebDriver webDriver;

    public static WebDriver buildWebDriver(String browserType) {
        switch (browserType.toUpperCase()) {
            case "CHROME":
                buildChromeDriver();
                break;
            case "FIREFOX":
                buildFireFoxDriver();
                break;
            case "EDGE":
                buildEdgeDriver();
                break;
        }
        return webDriver;
    }

    private static void buildChromeDriver() {
        capabilities = DesiredCapabilities.chrome();
        File chromeDriver = new File("src/main/resources/drivers/chromedriver.exe");
        ChromeDriverService chromeDriverService = new ChromeDriverService.Builder()
                .usingDriverExecutable(chromeDriver)
                .usingAnyFreePort()
                .build();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.merge(capabilities);
        webDriver = new ChromeDriver(chromeDriverService, chromeOptions);
    }

    private static void buildFireFoxDriver() {
        capabilities = DesiredCapabilities.firefox();
        File geckoDriver = new File("src/main/resources/drivers/geckodriver.exe");
        GeckoDriverService fireFoxDriverService = new GeckoDriverService.Builder()
                .usingDriverExecutable(geckoDriver)
                .usingAnyFreePort()
                .build();
        FirefoxOptions fireFoxOptions = new FirefoxOptions();
        fireFoxOptions.merge(capabilities);
        webDriver = new FirefoxDriver(fireFoxDriverService, fireFoxOptions);
    }

    private static void buildEdgeDriver() {
        capabilities = DesiredCapabilities.edge();
        File edgeDriver = new File("src/main/resources/drivers/MicrosoftWebDriver.exe");
        EdgeDriverService edgeDriverService = new EdgeDriverService.Builder()
                .usingDriverExecutable(edgeDriver)
                .usingAnyFreePort()
                .build();
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.merge(capabilities);
        webDriver = new EdgeDriver(edgeDriverService, edgeOptions);
    }
}
