package frontend.utilities;

import common.Configuration;
import org.openqa.selenium.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;


public class WebDriverManager {

    private Logger logger = Logger.getLogger(WebDriverManager.class.getName());
    private static final int IMPLICIT_WAIT_TIME = 5;

    private WebDriver webDriver;
    private List<String> openWindowHandles;

    public WebDriver getWebDriver() {
        if (null == webDriver) {
            webDriver = createDriver();
        }
        return webDriver;
    }

    private WebDriver createDriver() {
        logger.info("Creating new" + Configuration.getBrowserName() + " driver ");
        webDriver = WebDriverBuilder.buildWebDriver(Configuration.getBrowserName());
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        openWindowHandles = new ArrayList<>();
        openWindowHandles.add(webDriver.getWindowHandle());
        return webDriver;
    }

    public void switchToFrame(String webElementXpath) {
        webDriver.switchTo().frame(webDriver.findElement(By.xpath(webElementXpath)));
    }

}
