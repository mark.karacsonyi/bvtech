package common;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DatabaseConnection {

    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:.\\src\\main\\resources\\data\\database\\executeTimes.db";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public static void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }

    public static Long executeQuery(Connection connection, String testCaseName) throws SQLException {
        long runTime = 0L;
        String sql = "SELECT  RUNTIME FROM runTimes WHERE TEST_NAME = (?) order by EXECUTION_TIME DESC LIMIT 1  ";
        PreparedStatement pstmt = connection.prepareStatement(sql);
        pstmt.setString(1, testCaseName);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            runTime = rs.getLong("RUNTIME");
        }
        return runTime;
    }

    public static void executeUpdate(Connection connection, String testName, Long runTime) throws SQLException {
        PreparedStatement p = connection.prepareStatement("INSERT INTO runTimes(TEST_NAME,RUNTIME,EXECUTION_TIME) Values(?,?,?)");

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedString = localDateTime.format(formatter);
        p.setString(1, testName);
        p.setLong(2, runTime);
        p.setString(3, formattedString);
        p.executeUpdate();
    }

}
