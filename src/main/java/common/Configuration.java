package common;


import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class Configuration {

    private static final String envName = StringUtils.defaultString(System.getProperty("env"), "integration");
    private static final String browserName = StringUtils.defaultString(System.getProperty("browser"), "Chrome");
    private static final JSONObject configJson = readFromConfig();

    public static JSONObject readFromConfig() {

        JSONObject json = new JSONObject();
        JSONParser parser = new JSONParser();
        try {
            json = (JSONObject) parser.parse(new FileReader("src/main/resources/data/Configuration.json"));
            json = (JSONObject) json.get(envName);
        } catch (IOException | ParseException e) {
            e.printStackTrace();

        }
        return json;
    }

    public static String getApiUrl() {
        return (String) configJson.get("apiUrl");
    }

    public static String getWebUrl() {
        return (String) configJson.get("webUrl");
    }

    public static String getBrowserName() {
        return browserName;
    }
}
